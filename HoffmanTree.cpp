#include "HoffmanTree.h"


HoffmanTree::HoffmanTree(void)
{
}

HoffmanTree::HoffmanTree(unordered_map<char, int> elements)
{
	for(auto it = elements.begin(); it!= elements.end(); it++)
	{
		string word = "";
		word+= (*it).first;
		tree_node tn = {word, (*it).second, nullptr, nullptr};
		frequencyTable.push(tn);
	}
}
void HoffmanTree::constructTree()
{
	while(frequencyTable.size() > 1)
	{
		//Copy the nodes into a perm location for references.
		tree_node findFirst = frequencyTable.top();
		nodes[findFirst.words] = findFirst;
		frequencyTable.pop();
		tree_node findSecond = frequencyTable.top();
		nodes[findSecond.words] = findSecond;
		frequencyTable.pop();
		tree_node* first = &nodes[findFirst.words];
		tree_node* second = &nodes[findSecond.words];

		tree_node newlyAdded = { first->words+second->words, first->frequency+second->frequency, first, second}; 
		frequencyTable.push(newlyAdded);
	}
	root = &frequencyTable.top();
}
tree_node* HoffmanTree::getRoot()
{
	if(root)	
		return root;
	return nullptr;
}

HoffmanTree::~HoffmanTree(void)
{
}
