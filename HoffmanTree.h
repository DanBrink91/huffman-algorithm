#pragma once

#include <string>
#include <queue>
#include <unordered_map>
using namespace std;

struct tree_node
{
	string words;
	int frequency;
	tree_node* left;
	tree_node* right;
};

	struct node_compare {
		bool operator()(tree_node a, tree_node b){
			return a.frequency > b.frequency;
		}
	};
class HoffmanTree
{
public:
	HoffmanTree(void);
	HoffmanTree(unordered_map<char, int> elements);
	~HoffmanTree(void);

	void constructTree();
	tree_node* getRoot();
	
	priority_queue<tree_node, deque<tree_node>, node_compare> frequencyTable;
private:
	unordered_map<string, tree_node> nodes;
	tree_node* root;
};

