// Decoder.cpp : Decodes the input file with the key of key.txt
//

#include <iostream>
#include <fstream>
#include <streambuf>
#include <string>
#include <queue>
#include <unordered_map>

#include "TextAnalyzer.h"
#include "HoffmanTree.h"
#include "Encoder.h"
using namespace std;

int main(int argc, char* argv[])
{
	string filename = string(argv[1]);
	Encoder encoder;
	//cout << "Loading Key..." << endl;
	encoder.loadKey("key.txt");
	//cout << "Decoding File..." << endl;
	encoder.decodeFile(filename.c_str(), "decoded.txt");
	return 0;
}
