// Huffman.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <fstream>
#include <streambuf>
#include <string>
#include <queue>
#include <unordered_map>

#include "TextAnalyzer.h"
#include "HoffmanTree.h"
#include "Encoder.h"
using namespace std;

int main(int argc, char* argv[])
{
	string filename = string(argv[1]);

	TextAnalyzer ta;
	ta.getFrequencies(filename.c_str());

	HoffmanTree ht(ta.elements);
	ht.constructTree();

	tree_node* root = ht.getRoot();

	Encoder encoder;
	//cout << "Encoding File..." << endl;
	encoder.encodeFile(filename.c_str(), root);
	return 0;
}
