#include "TextAnalyzer.h"


TextAnalyzer::TextAnalyzer(void)
{
}


TextAnalyzer::~TextAnalyzer(void)
{
}

void TextAnalyzer::getFrequencies(const char* filename)
{
	ifstream inputFile;
	inputFile.open(filename, ios::in);
	if(!inputFile)
	{
		cout << "File: " << filename <<" couldn't be opened." << endl;
	}
	string text((istreambuf_iterator<char>(inputFile)), istreambuf_iterator<char>());
	for(int i = 0; i < text.length(); i++)
	{
		auto it = elements.find(text[i]);
		if(it != elements.end())
		{
			elements[text[i]] += 1;
		}
		else
		{
			elements[text[i]] = 1;
		}
	}
	inputFile.close();
}