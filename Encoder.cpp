#include "Encoder.h"


Encoder::Encoder(void)
{
}


Encoder::~Encoder(void)
{
}
string Encoder::encode(tree_node* root,char target)
{
	tree_node* cur  = root;
	string encoded = "";
	while(cur!=nullptr)
	{
		if(cur->words[0]==target && cur->words.length()==1)
			return encoded;
		//size_t inStr = ;
		if(cur->left->words.find(target)!= cur->left->words.npos)
		{
			encoded+='0';
			cur = cur->left;
		}
		else
		{
			encoded+='1';
			cur=cur->right;
		}
	}	
	return encoded;
}
void Encoder::encodeFile(const char* filename, tree_node* root)
{
	ofstream outFile;
	outFile.open("output.txt", ios::out);
	ifstream inputFile;
	inputFile.open(filename, ios::in);
	if(!inputFile)
	{
	}
	string text((istreambuf_iterator<char>(inputFile)), istreambuf_iterator<char>());
	inputFile.close();
	for(int i = 0; i < text.length(); i++)
	{
		unordered_map<char, string>::const_iterator findKey = encoded.find(text[i]);
		if(findKey!=encoded.end())
		{
			cout << encoded[text[i]];
		}
		else
		{
			string to = encode(root, text[i]);
			encoded[text[i]] = to;
			cout << to;
		}
	}
	outFile.close();
	outFile.open("key.txt", ios::out);
	for(auto it = encoded.begin(); it!=encoded.end(); it++)
	{
		outFile << (*it).first << " => " << (*it).second << endl;
	}
	outFile.close();
}

void Encoder::loadKey(const char* filename)
{
	ifstream inputFile;
	inputFile.open(filename, ios::in);
	if(!inputFile)
	{
	}
	string text((istreambuf_iterator<char>(inputFile)), istreambuf_iterator<char>());
	
	char letter;
	for(int i = 0; i < text.length(); i++)
	{
		letter = text[i];
		i+=5; //Skip over the " => "
		string code = "";
		while(text[i]=='1' || text[i]=='0')
		{
			code+= text[i];
			i++;
		}
		key[code] = letter;
	}
	inputFile.close();
}

void Encoder::decodeFile(const char* filename, string output)
{
	ifstream inputFile;
	inputFile.open(filename, ios::in);
	ofstream outFile;
	outFile.open(output.c_str(), ios::out);
	string text((istreambuf_iterator<char>(inputFile)), istreambuf_iterator<char>());
	for(int i = 0; i < text.length(); i++)
	{
		string code = "";
		while(i < text.length())
		{
			code += text[i];
			unordered_map<string, char>::const_iterator findKey = key.find(code);
			if(findKey!=key.end())
			{
				cout << key[code];
				break;
			}
			i++;
		}
		//i--;
	}
}