#pragma once
#include <iostream>
#include <fstream>
#include <streambuf>
#include <string>
#include <queue>
#include <unordered_map>

using namespace std;

typedef struct
{
	char word;
	int frequency;
} element;

class TextAnalyzer
{
public:
	TextAnalyzer(void);
	~TextAnalyzer(void);
	void getFrequencies(const char* filename);

	unordered_map<char, int> elements;
};

