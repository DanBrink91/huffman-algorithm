#pragma once
#include <string>
#include <unordered_map>
#include "HoffmanTree.h"

#include <fstream>
#include <iostream>
using namespace std;
class Encoder
{
public:
	Encoder(void);
	~Encoder(void);

	void encodeFile(const char* filename, tree_node* root);
	void loadKey(const char* filename);
	void decodeFile(const char* filename, string outFile);
private:
	string encode(tree_node* root,char target);
	unordered_map<char, string> encoded;
	unordered_map<string, char> key;
};

