objects = Encoder.o TextAnalyzer.o HoffmanTree.o
all: encoder decoder

encoder: Huffman.o $(objects)
	g++ -o encoder $(objects) Huffman.o
decoder: Decoder.o $(objects)
	g++ -o decoder $(objects) Decoder.o
Huffman.o:
	g++ -c Huffman.cpp -std=c++11
Decoder.o:
	g++ -c Decoder.cpp -std=c++11
Encoder.o: HoffmanTree.o
	g++ -c Encoder.cpp HoffmanTree.o -std=c++11
TextAnalyzer.o:
	g++ -c TextAnalyzer.cpp -std=c++11
HoffmanTree.o:
	g++ -c HoffmanTree.cpp -std=c++11 -fpermissive
clean:
	rm Encoder.o TextAnalyzer.o HoffmanTree.o huffman.o encoder decoder.o decoder
